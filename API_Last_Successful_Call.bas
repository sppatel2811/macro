Attribute VB_Name = "API_Last_Successful_Call"
Public Sub PMSFileLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet
Dim WrkShtHist As Worksheet
Dim LastRow As Long
'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
Set WrkShtHist = ThisWorkbook.Worksheets("History Logs")

    WrkSht.Cells(1, 7).Value = Now
    LastRow = WrkShtHist.Cells(WrkShtHist.Rows.Count, "A").End(xlUp).Row
    WrkShtHist.Cells(LastRow + 1, 1).Value = Now
End Sub

Public Sub PeriodRangeLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")

    WrkSht.Cells(2, 7).Value = Now
    
End Sub

Public Sub ComplaintsLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht, WrkShtHist As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
Set WrkShtHist = ThisWorkbook.Worksheets("History Logs")

    
    LastRow = WrkShtHist.Cells(WrkShtHist.Rows.Count, "E").End(xlUp).Row
    WrkShtHist.Cells(LastRow + 1, 5).Value = Now
    WrkSht.Cells(3, 7).Value = Now
    
End Sub

Public Sub SalesLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht, WrkShtHist As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
Set WrkShtHist = ThisWorkbook.Worksheets("History Logs")

    
    LastRow = WrkShtHist.Cells(WrkShtHist.Rows.Count, "D").End(xlUp).Row
    WrkShtHist.Cells(LastRow + 1, 4).Value = Now
    WrkSht.Cells(4, 7).Value = Now
    
End Sub

Public Sub PartsLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht, WrkShtHist As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
Set WrkShtHist = ThisWorkbook.Worksheets("History Logs")

    
    LastRow = WrkShtHist.Cells(WrkShtHist.Rows.Count, "C").End(xlUp).Row
    WrkShtHist.Cells(LastRow + 1, 3).Value = Now
    WrkSht.Cells(5, 7).Value = Now
    
End Sub
Public Function PMSFileNameLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")

    WrkSht.Cells(6, 7).Value = GetUserSelectedPMSFile()
    
End Function
Public Function StartDateLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")

    WrkSht.Cells(7, 7).Value = GetUserSelectedStartDate()
    
End Function
Public Function EndDateLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")

    WrkSht.Cells(8, 7).Value = GetUserSelectedEndDate()
    
End Function
Public Function DataTypeLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")

    WrkSht.Cells(9, 7).Value = GetUserSelectedDataType()
    
End Function

Public Function GetPMSFileLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
    GetPMSFileLastUpdated = WrkSht.Range("G1").Value
End Function

Public Function GetPeriodRangeLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
    GetPeriodRangeLastUpdated = WrkSht.Range("G2").Value
End Function

Public Function GetComplaintsLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
    GetComplaintsLastUpdated = WrkSht.Range("G3").Value
End Function

Public Function GetSalesLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
    GetSalesLastUpdated = WrkSht.Range("G4").Value
End Function

Public Function GetPartsLastUpdated()

'Define the worksheet that will house the data
Dim WrkSht As Worksheet

'Workbook sheet name
Set WrkSht = ThisWorkbook.Worksheets("Master Data")
    GetPartsLastUpdated = WrkSht.Range("G5").Value
End Function
